﻿namespace Web_Project.Models
{
    public class ErrorCheck
    {
        public bool Result { get; set; }
        public List<string> Errors { get; set; }
        public List<string> Message { get; set; }
    }
}
