﻿using BusinessLogic.Services.ShoppingCart;
using DataLayer.Entityes;
using DataLayer.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;

namespace Web_Project.Controllers
{
    public class CartController : Controller
    {
        private readonly ICartService _service;
        public CartController(ICartService service)
        {
            _service = service;

        }

        public async Task<IActionResult> Index()
        {
            string token;
            HttpContext.Request.Cookies.TryGetValue("Authorization", out token);

            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(token);
            var tokenS = jsonToken as JwtSecurityToken;
            int userId = int.Parse(tokenS.Claims.First(claim => claim.Type == "userId").Value);

            Cart cart = await _service.GetCart(userId);
            var items = _service.GetAllItems(cart.Id);

            var response = new ShoppingCartVM()
            {
                CartItems = _service.GetAllItems(cart.Id),
                Total = _service.GetCartTotal(cart.Id)
            };

            return View(response);
        }

        public async Task<IActionResult> AddToCart(int id, string method, string path)
        {
            string token;
            HttpContext.Request.Cookies.TryGetValue("Authorization", out token);

            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(token);
            var tokenS = jsonToken as JwtSecurityToken;
            int userId = int.Parse(tokenS.Claims.First(claim => claim.Type == "userId").Value);

            Cart cart = await _service.GetCart(userId);
            await _service.AddNewItem(cart.Id, id);
            return RedirectToAction(method, path);
        }

        public async Task<IActionResult> RemoveFromCart(int id)
        {
            string token;
            HttpContext.Request.Cookies.TryGetValue("Authorization", out token);

            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(token);
            var tokenS = jsonToken as JwtSecurityToken;
            int userId = int.Parse(tokenS.Claims.First(claim => claim.Type == "userId").Value);

            Cart cart = await _service.GetCart(userId);
            await _service.RemoveItem(cart.Id, id);
            return RedirectToAction("Index", "Cart");
        }

        public async Task<IActionResult> CleanCart()
        {
            string token;
            HttpContext.Request.Cookies.TryGetValue("Authorization", out token);

            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(token);
            var tokenS = jsonToken as JwtSecurityToken;
            int userId = int.Parse(tokenS.Claims.First(claim => claim.Type == "userId").Value);

            Cart cart = await _service.GetCart(userId);
            await _service.CleanItems(cart.Id);
            return RedirectToAction("Index", "Cart");
        }
    }
}
