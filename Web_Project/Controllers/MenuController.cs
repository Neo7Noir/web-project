﻿using BusinessLogic.Services;
using DataLayer.Enums;
using DataLayer.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;

namespace Web_Project.Controllers
{
    public class MenuController : Controller
    {
        private readonly IMenuService _service;
        public MenuController(IMenuService service)
        {
            _service = service;
        }

        public async Task<IActionResult> Index()
        {
            var menues = await _service.GetAll();
            return View(menues);
        }

        public async Task<ActionResult> FilterView(string category)
        {
            if(category != null)
            {
                BaseCategories myEnum = (BaseCategories)Enum.Parse(typeof(BaseCategories), category);
                var menuList = await _service.GetByCategory(myEnum);
                return PartialView(menuList);
            }
            else
            {
                var menuList = await _service.GetAll();
                return PartialView(menuList);
            }
        }

        public List<SelectListItem> GetRestaurants()
        {
            var lstRest = new List<SelectListItem>();
            var restaurants = _service.GetRestaurants();

            lstRest = restaurants.Select(c => new SelectListItem()
            {
                Value = c.Id.ToString(),
                Text = c.Name,
            }).ToList();

            var restItem = new SelectListItem()
            {
                Value = "",
                Text = "Select Restaurant"
            };

            lstRest.Insert(0, restItem);


            return lstRest;
        }

        public List<SelectListItem> GetProducts()
        {
            var lstProd = new List<SelectListItem>();
            var products = _service.GetProducts();

            lstProd = products.Select(c => new SelectListItem()
            {
                Value = c.Id.ToString(),
                Text = c.Name,
            }).ToList();

            var restItem = new SelectListItem()
            {
                Value = "",
                Text = "Select Products"
            };

            lstProd.Insert(0, restItem);


            return lstProd;
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            ViewBag.RestId = GetRestaurants();
            ViewBag.ProdId = GetProducts();
            SelectList categories = new SelectList(Enum.GetValues(typeof(BaseCategories)));
            ViewBag.Categories = categories;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateMenuVM menu)
        {
            if(ModelState.IsValid)
            {
                await _service.Add(menu);
                return RedirectToAction(nameof(Index));
            }
            ViewBag.RestId = GetRestaurants();
            ViewBag.ProdId = GetProducts();
            SelectList categories = new SelectList(Enum.GetValues(typeof(BaseCategories)));
            ViewBag.Categories = categories;
            return View(menu);
        }

        public async Task<IActionResult> Details(int id)
        {
            var menuDetail = await _service.GetById(id);
            return View(menuDetail);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var menuDetails = await _service.GetById(id);

            var response = new CreateMenuVM()
            {
                Id = menuDetails.Id,
                Name = menuDetails.Name,
                Price = menuDetails.Price,
                PictureURL = menuDetails.PictureURL,
                Category = menuDetails.Category,
                RestaurantId = menuDetails.RestaurantId,
                ProductIds = menuDetails.Products_Menues.Select(m => m.ProductId).ToList()
            };

            ViewBag.RestId = GetRestaurants();
            ViewBag.ProdId = GetProducts();
            SelectList categories = new SelectList(Enum.GetValues(typeof(BaseCategories)));
            ViewBag.Categories = categories;
            return View(response);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CreateMenuVM menu)
        {
            if (ModelState.IsValid)
            {
                await _service.Update(menu);
                return RedirectToAction(nameof(Index));
            }
            ViewBag.RestId = GetRestaurants();
            ViewBag.ProdId = GetProducts();
            SelectList categories = new SelectList(Enum.GetValues(typeof(BaseCategories)));
            ViewBag.Categories = categories;
            return View(menu);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
