﻿using BusinessLogic.Services;
using DataLayer.Enums;
using Microsoft.AspNetCore.Mvc;
using NuGet.Common;
using System.Diagnostics;
using System.Dynamic;
using System.IdentityModel.Tokens.Jwt;
using Web_Project.Models;

namespace Web_Project.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMenuService _service;

        public HomeController(ILogger<HomeController> logger, IMenuService service)
        {
            _logger = logger;
            _service = service;
        }

        public async Task<ActionResult> Index(bool? wasRedirected)
        {
            if (wasRedirected == true) 
            {
                ViewData["Login"] = "Succesfully Logged In";

                string token;
                HttpContext.Request.Cookies.TryGetValue("Authorization", out token);
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = jsonToken as JwtSecurityToken;
                string username = tokenS.Claims.First(claim => claim.Type == "sub").Value;
                string isAdmin = tokenS.Claims.First(claim => claim.Type == "Role").Value;

                ViewData["Token"] = token;
                ViewData["Username"] = username;
                if(isAdmin == "admin")
                {
                    return RedirectToAction("Index", "Menu");
                }
            }
            ViewData["Categories"] = Enum.GetValues(typeof(BaseCategories)).Cast<BaseCategories>();
            var menues = await _service.GetAll();
            return View(menues);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}