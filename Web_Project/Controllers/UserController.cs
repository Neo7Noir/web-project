﻿using BusinessLogic.Services.User;
using DataLayer.User;
using DataLayer.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Web_Project.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _service;
        public UserController(IUserService service)
        {
            _service = service;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(CreateUserVM user) 
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _service.AddUser(user);
                    await _service.AddCart(user);
                    return RedirectToAction(nameof(Login), new { wasRedirected = true });
                }
                //Implement Token

                return View(user);
            }catch(Exception error) 
            {
                ViewData["Error"] = error.Message;
                return View(user);
            }
        }

        [HttpGet]
        public IActionResult Login(bool? wasRedirected)
        {
            if (wasRedirected == true)
            {
                ViewData["Register"] = "Succesfully Registered";
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginUserVM user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    UserData UserExist = await _service.CheckUser(user);
                    UserData data = await _service.CheckEmail(user.Email);
                    var token = _service.GenerateToken(data);
                    HttpContext.Response.Cookies.Append("Authorization", token, new CookieOptions { HttpOnly = true, Secure = true });
                    return RedirectToAction("Index", "Home", new { wasRedirected = true });
                }
                //Implement Token

                return View(user);
            }catch(Exception error)
            {
                ViewData["Error"] = error.Message;
                return View(user);
            }
        }

        public IActionResult Logout()
        {
            HttpContext.Response.Cookies.Delete("Authorization");
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Users()
        {
            var users = await _service.GetAll();
            return View(users);

        }

    }
}
