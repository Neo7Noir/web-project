﻿using BusinessLogic.Services;
using DataLayer.Entityes;
using DataLayer.Enums;
using Microsoft.AspNetCore.Mvc;
using System.Dynamic;

namespace Web_Project.Controllers
{
    public class RestaurantController : Controller
    {
        private readonly IRestaurantService _service;

        public RestaurantController(IRestaurantService service)
        {
            _service = service;
        }
        public async Task<IActionResult> Index()
        {
            var restaurants = await _service.GetAll();
            return View(restaurants);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Restaurant restaurant)
        {
            if (ModelState.IsValid)
            {
                await _service.Add(restaurant);
                return RedirectToAction(nameof(Index));
            }
            return View(restaurant);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var restaurantDetails = await _service.GetById(id);

            var response = new Restaurant()
            {
                Id = restaurantDetails.Id,
                Name = restaurantDetails.Name,
                PictureURL = restaurantDetails.PictureURL,
                Category = restaurantDetails.Category,
            };

            return View(response);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Restaurant restaurant)
        {
            if (ModelState.IsValid)
            {
                await _service.Update(restaurant);
                return RedirectToAction(nameof(Index));
            }

            return View(restaurant);
        }

        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            dynamic model = new ExpandoObject();
            model.Restaurant = await _service.GetAsList(id);
            model.Menu = await _service.GetAllMenues();
            return View(model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
