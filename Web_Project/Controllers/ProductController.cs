﻿using BusinessLogic.Services;
using DataLayer.Entityes;
using DataLayer.Enums;
using DataLayer.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Web_Project.Controllers
{
    public class ProductController : Controller
    {   
        private readonly IProductService _service;

        public ProductController(IProductService service)
        {
            _service = service;
        }
        public async Task<IActionResult> Index()
        {
            var products = await _service.GetAll();
            return View(products);
        }

        //Get: Product/Create
        public IActionResult Create()
        {
            SelectList categories = new SelectList(Enum.GetValues(typeof(ProductCategories)));
            ViewBag.Categories = categories;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create([Bind("Id, Name, PictureURL, Description, Category, Price")]Product product)
        {
            if (ModelState.IsValid)
            {
                await _service.Add(product);
                return RedirectToAction(nameof(Index));
            }
            SelectList categories = new SelectList(Enum.GetValues(typeof(ProductCategories)));
            ViewBag.Categories = categories;
            return View(product);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var productDetails = await _service.GetById(id);

            var response = new Product()
            {
                Id = productDetails.Id,
                Name = productDetails.Name,
                PictureURL = productDetails.PictureURL,
                Description = productDetails.Description,
                Category = productDetails.Category,
                Price = productDetails.Price,
            };

            SelectList categories = new SelectList(Enum.GetValues(typeof(ProductCategories)));
            ViewBag.Categories = categories;
            return View(response);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                await _service.Update(product);
                return RedirectToAction(nameof(Index));
            }

            SelectList categories = new SelectList(Enum.GetValues(typeof(ProductCategories)));
            ViewBag.Categories = categories;
            return View(product);
        }

        public async Task<IActionResult> Details(int id)
        {
            var productDetail = await _service.GetById(id);
            return View(productDetail);
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _service.Delete(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
