﻿using DataLayer.Entityes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BusinessLogic.Services
{
    public interface IRestaurantService
    {
        Task<IEnumerable<Restaurant>> GetAll();

        Task<IEnumerable<Menu>> GetAllMenues();

        Task<Restaurant> GetById(int id);
        Task<IEnumerable<Restaurant>> GetAsList(int id);
        Task Add(Restaurant restaurant);
        Task Update(Restaurant newRestaurant);
        Task Delete(int id);

        Task DeleteRelations(int id);
    }
}
