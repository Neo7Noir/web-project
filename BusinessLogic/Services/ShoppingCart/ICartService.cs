﻿using DataLayer.Entityes;
using DataLayer.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services.ShoppingCart
{
    public interface ICartService
    {
        List<CartItem> GetAllItems(int cartId);
        Task AddNewItem(int cartId, int itemId);
        Task RemoveItem(int cartId, int itemId);
        Task CleanItems(int cartId);
        Task<Cart> GetCart(int id);
        double GetCartTotal(int cartId);
    }
}
