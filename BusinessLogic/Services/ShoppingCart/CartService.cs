﻿using DataLayer;
using DataLayer.Entityes;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services.ShoppingCart
{
    public class CartService : ICartService
    {
        private readonly AppDbContext _context;
        public CartService(AppDbContext context)
        {
            _context = context;
        }
        public async Task AddNewItem(int cartId, int itemId)
        {
            var cartItem = await _context.CartItems.FirstOrDefaultAsync(x => x.CartId == cartId && x.MenuId == itemId);

            if(cartItem == null)
            {
                cartItem = new CartItem()
                {
                    CartId = cartId,
                    MenuId = itemId,
                    Amount = 1,
                };
                await _context.CartItems.AddAsync(cartItem);
            }
            else
            {
                cartItem.Amount++;
            }

            await _context.SaveChangesAsync();
        }

        public List<CartItem> GetAllItems(int cartId)
        {
            var items = _context.CartItems.Where(m =>m.CartId == cartId).Include(m => m.Menu).ToList();
            return items;
        }

        public async Task<Cart> GetCart(int id)
        {
            var cart = await _context.Cart
                                .FirstOrDefaultAsync(m => m.UserId == id);

            return cart;
        }

        public async Task RemoveItem(int cartId, int itemId)
        {
            var cartItem = await _context.CartItems.FirstOrDefaultAsync(x => x.CartId == cartId && x.MenuId == itemId);

            if (cartItem.Amount == 1)
            {
                _context.CartItems.Remove(cartItem);
            }
            else
            {
                cartItem.Amount--;
            }

            await _context.SaveChangesAsync();
        }

        public async Task CleanItems(int cartId)
        {
            var cartItems = await _context.CartItems.Where(x => x.CartId == cartId).ToListAsync();
            _context.CartItems.RemoveRange(cartItems);
            await _context.SaveChangesAsync();
        }

        public double GetCartTotal(int cartId) => _context.CartItems.Where(n => n.CartId == cartId).Select(n => n.Menu.Price * n.Amount).Sum();
    }
}
