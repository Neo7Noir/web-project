﻿using DataLayer.Entityes;
using DataLayer.Enums;
using DataLayer.ViewModels;

namespace BusinessLogic.Services
{
    public interface IMenuService
    {
        Task<IEnumerable<Menu>> GetAll();

        Task<IEnumerable<Menu>> GetByCategory(BaseCategories category);

        List<Restaurant> GetRestaurants();

        List<Product> GetProducts();

        Task<Menu> GetById(int id);

        Task Add(CreateMenuVM menu);

        Task Update(CreateMenuVM newMenu);

        Task Delete(int id);
    }
}
