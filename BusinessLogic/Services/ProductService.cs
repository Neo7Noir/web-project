﻿using DataLayer;
using DataLayer.Entityes;
using Microsoft.EntityFrameworkCore;


namespace BusinessLogic.Services
{
    public class ProductService : IProductService
    {
        private readonly AppDbContext _context;
        public ProductService(AppDbContext context)
        {
            _context = context;
        }

        public async Task Add(Product product)
        {
            await _context.Products.AddAsync(product);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Product>> GetAll()
        {
            var products = await _context.Products.ToListAsync();
            return products;
        }

        public async Task<Product> GetById(int id)
        {
            var productDetails = await _context.Products
                .FirstOrDefaultAsync(m => m.Id == id);

            return productDetails;
        }

        public async Task Update(Product newProduct)
        {
            var product = await _context.Products.FindAsync(newProduct.Id);

            if (product != null)
            {
                product.Id = product.Id;
                product.Name = newProduct.Name;
                product.PictureURL = newProduct.PictureURL;
                product.Description= newProduct.Description;
                product.Price = newProduct.Price;
                product.Category = product.Category;
                await _context.SaveChangesAsync();
            }

            //remove Product_Menues
            var existingConnect = _context.Products_Menues.Where(m => m.ProductId == newProduct.Id).ToList();
            _context.Products_Menues.RemoveRange(existingConnect);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product != null)
            {
                var existingConnect = _context.Products_Menues.Where(m => m.ProductId == id).ToList();
                _context.Products_Menues.RemoveRange(existingConnect);

                _context.Products.Remove(product);
                await _context.SaveChangesAsync();
            }
        }
    }
}
