﻿using DataLayer.Entityes;
using DataLayer.User;
using DataLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services.User
{
    public interface IUserService
    {
        Task<IEnumerable<UserData>> GetAll();
        Task AddUser(CreateUserVM user);
        Task AddCart(CreateUserVM data);
        Task<UserData> CheckEmail(string email);
        Task<UserData> CheckUser(LoginUserVM user);
        Task<UserData> GetUser(int id);
        string GenerateToken(UserData request);
    }
}
