﻿using DataLayer;
using DataLayer.Entityes;
using DataLayer.User;
using DataLayer.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace BusinessLogic.Services.User
{
    public class UserService : IUserService
    {
        private readonly AppDbContext _context;
        private const string TokenSecret = "asflags;lasdTestKey";
        private static readonly TimeSpan TokenLifeTime = TimeSpan.FromHours(1);
        public UserService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<UserData>> GetAll()
        {
            var users = await _context.Users.ToListAsync();
            return users;
        }
        public async Task AddUser(CreateUserVM user)
        {
            UserData userExist = await CheckEmail(user.Email);
            if(userExist != null)
            {
                throw new Exception("Email is already registered");
            }

            if(user.Password != user.ConfirmPass)
            {
                throw new Exception("Passwords are not identical!");
            }

            string hashPass = HashPassword(user.Password);

            var newUser = new UserData()
            {
                Email = user.Email,
                Password = hashPass,
                Username = user.Username,
            };

            await _context.Users.AddAsync(newUser);
            await _context.SaveChangesAsync();
        }

        public async Task AddCart(CreateUserVM data)
        {
            UserData user = await CheckEmail(data.Email);

            Cart userCart = new Cart()
            {
                UserId = user.Id
            };
            await _context.Cart.AddAsync(userCart);
            await _context.SaveChangesAsync();
        }


        public async Task<UserData> CheckEmail(string email)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Email== email);
        }

        public async Task<UserData> CheckUser(LoginUserVM user)
        {
            var userExist = await CheckEmail(user.Email);

            if (userExist == null)
            {
                throw new Exception("Invalid email!");
            }

            if (HashPassword(user.Password) != userExist.Password)
            {
                throw new Exception("Invalid password!");
            }

            return userExist;

        }

        public async Task<UserData> GetUser(int id)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);

            return user;
        }

        public string GenerateToken(UserData request)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(TokenSecret);

            var claims = new List<Claim>
            {
                new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new(JwtRegisteredClaimNames.Sub, request.Username),
                new(JwtRegisteredClaimNames.Email, request.Email),
                new("userId", request.Id.ToString()),
                new("Role", request.Role.ToString())
            };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.Add(TokenLifeTime),
                Issuer = "",
                Audience = "",
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256),
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var jwt = tokenHandler.WriteToken(token);
            return jwt;
        }

        private string HashPassword(string password)
        {
            SHA256 hash = SHA256.Create();
            var passwordBytes = Encoding.Default.GetBytes(password);
            var hashedPassword = hash.ComputeHash(passwordBytes);

            return Convert.ToHexString(hashedPassword);
        }

    }
}
