﻿using DataLayer;
using DataLayer.Entityes;
using Microsoft.EntityFrameworkCore;
using System.Web.Mvc;

namespace BusinessLogic.Services
{
    public class RestaurantService : IRestaurantService
    {
        private readonly AppDbContext _context;
        public RestaurantService(AppDbContext context)
        {
            _context = context;
        }

        public async Task Add(Restaurant restaurant)
        {
            await _context.Restaurants.AddAsync(restaurant);
            await _context.SaveChangesAsync();
        }


        public async Task<IEnumerable<Restaurant>> GetAll()
        {
            var restaurants = await _context.Restaurants.ToListAsync();
            return restaurants;
        }

        public async Task<IEnumerable<Menu>> GetAllMenues()
        {
            var menues = await _context.Menus.ToListAsync();
            return menues;
        }

        public async Task<Restaurant> GetById(int id)
        {
            var restaurantDetails = await _context.Restaurants
                .FirstOrDefaultAsync(m => m.Id == id);

            return restaurantDetails;
        }

        public async Task<IEnumerable<Restaurant>> GetAsList(int id)
        {

            var restaurantDetails = await _context.Restaurants.Where(m => m.Id == id).ToListAsync();

            return restaurantDetails;
        }

        public async Task Update(Restaurant newRestaurant)
        {
            var restaurant = await _context.Restaurants.FindAsync(newRestaurant.Id);

            if (restaurant != null)
            {
                restaurant.Id = restaurant.Id;
                restaurant.Name = newRestaurant.Name;
                restaurant.PictureURL = newRestaurant.PictureURL;
                restaurant.Category = restaurant.Category;
                await _context.SaveChangesAsync();
            }
        }

        public async Task DeleteRelations(int id)
        {
            var menu = await _context.Menus.FindAsync(id);
            if (menu != null)
            {
                var existingConnect = _context.Products_Menues.Where(m => m.MenuId == id).ToList();
                _context.Products_Menues.RemoveRange(existingConnect);

                _context.Menus.Remove(menu);
                await _context.SaveChangesAsync();
            }
        }

        public async Task Delete(int id)
        {
            var restaurant = await _context.Restaurants.FindAsync(id);
            if (restaurant != null)
            {
                var existingConnect = _context.Menus.Where(m => m.RestaurantId == id).ToList();
                foreach(var menu in existingConnect)
                {
                    await DeleteRelations(menu.Id);
                }

                _context.Restaurants.Remove(restaurant);
                await _context.SaveChangesAsync();
            }
        }
    }
}
