﻿using DataLayer;
using DataLayer.Entityes;
using DataLayer.Enums;
using DataLayer.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services
{
    public class MenuService : IMenuService
    {
        private readonly AppDbContext _context;
        public MenuService(AppDbContext context)
        {
            _context = context;
        }

        public List<Restaurant> GetRestaurants()
        {
            List<Restaurant> restaurants = _context.Restaurants.ToList();

            return restaurants;
        }

        public List<Product> GetProducts()
        {
            List<Product> products = _context.Products.ToList();

            return products;
        }

        public async Task Add(CreateMenuVM menu)
        {
            var newMenu = new Menu()
            {
                Id = menu.Id,
                Name = menu.Name,
                Price = menu.Price,
                PictureURL = menu.PictureURL,
                RestaurantId = menu.RestaurantId,
                Category = menu.Category,
            };
            await _context.Menus.AddAsync(newMenu);
            await _context.SaveChangesAsync();
            
            //Add Product_Menu
            foreach(var ProductId in menu.ProductIds)
            {
                var product_Menu = new Product_Menu()
                {
                    MenuId = menu.Id,
                    ProductId = ProductId
                };
                await _context.Products_Menues.AddAsync(product_Menu);
            }
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Menu>> GetAll()
        {
            var menues = await _context.Menus.ToListAsync();
            return menues;
        }

        public async Task<IEnumerable<Menu>> GetByCategory(BaseCategories category)
        {
            var menues = await _context.Menus.Where(m => m.Category == category).ToListAsync();
            return menues;
        }

        public async Task<Menu> GetById(int id)
        {
            var menuDetails = await _context.Menus
                .Include(r => r.Restaurant)
                .Include(pm => pm.Products_Menues).ThenInclude(p => p.Product)
                .FirstOrDefaultAsync(m => m.Id == id);

            return menuDetails;
        }

        public async Task Update(CreateMenuVM newMenu)
        {
            var menu = await _context.Menus.FindAsync(newMenu.Id);

            if(menu != null)
            {
                menu.Id = menu.Id;
                menu.Name = newMenu.Name;
                menu.PictureURL = newMenu.PictureURL;
                menu.Price = newMenu.Price;
                menu.RestaurantId = newMenu.RestaurantId;
                menu.Category = newMenu.Category;
                await _context.SaveChangesAsync();
            }

            //remove Product_Menues
            var existingConnect = _context.Products_Menues.Where(m => m.MenuId == newMenu.Id).ToList();
            _context.Products_Menues.RemoveRange(existingConnect);
            await _context.SaveChangesAsync();

            //add new Product_menues
            foreach (var ProductId in newMenu.ProductIds)
            {
                var product_Menu = new Product_Menu()
                {
                    MenuId = newMenu.Id,
                    ProductId = ProductId
                };
                await _context.Products_Menues.AddAsync(product_Menu);
            }
            await _context.SaveChangesAsync();
        }
        public async Task Delete(int id)
        {
            var menu = await _context.Menus.FindAsync(id);
            if(menu != null)
            {
                var existingConnect = _context.Products_Menues.Where(m => m.MenuId == id).ToList();
                _context.Products_Menues.RemoveRange(existingConnect);

                _context.Menus.Remove(menu);
                await _context.SaveChangesAsync();
            }

        }
    }
}
