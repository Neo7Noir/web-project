﻿using DataLayer.Entityes;
using DataLayer.User;
using Microsoft.EntityFrameworkCore;

//initialize DB
namespace DataLayer
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product_Menu>().HasKey(pm => new
            {
                pm.ProductId,
                pm.MenuId
            });

            modelBuilder.Entity<Product_Menu>().HasOne(m => m.Menu).WithMany(pm => pm.Products_Menues).HasForeignKey(m => m.MenuId);
            modelBuilder.Entity<Product_Menu>().HasOne(m => m.Product).WithMany(pm => pm.Products_Menues).HasForeignKey(m => m.ProductId);


            modelBuilder.Entity<CartItem>().HasKey(pm => new
            {
                pm.CartId,
                pm.MenuId
            });

            modelBuilder.Entity<CartItem>().HasOne(m => m.Cart).WithMany(pm => pm.CartItems).HasForeignKey(m => m.CartId);
            modelBuilder.Entity<CartItem>().HasOne(m => m.Menu).WithMany(pm => pm.CartItems).HasForeignKey(m => m.MenuId);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Product_Menu> Products_Menues { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<UserData> Users { get; set; }
        public DbSet<Cart> Cart { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
    }
}
