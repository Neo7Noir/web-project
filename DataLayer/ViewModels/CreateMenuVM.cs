﻿using DataLayer.Enums;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.ViewModels
{
    public class CreateMenuVM
    {
        public int Id { get; set; }

        [Display(Name = "Menu name")]
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Display(Name = "Price in $")]
        [Required(ErrorMessage = "Price is required")]
        public double Price { get; set; }

        [Display(Name = "PictureURL")]
        [Required(ErrorMessage = "PictureURL is required")]
        public string PictureURL { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Category is required")]
        public BaseCategories Category { get; set; }

        //Relationships
        [Display(Name = "Select product(s)")]
        [Required(ErrorMessage = "Product is required")]
        public List<int> ProductIds { get; set; }

        [Display(Name = "Select a Restaurant")]
        [Required(ErrorMessage = "Restaurant is required")]
        public int RestaurantId { get; set; }
    }
}
