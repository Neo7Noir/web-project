﻿using DataLayer.Entityes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.ViewModels
{
    public class ShoppingCartVM
    {
        public List<CartItem> CartItems { get; set; }
        public double Total { get; set; }
    }
}
