﻿using DataLayer.Entityes;
using DataLayer.User;

namespace DataLayer
{
    public class DBInitializer
    {
        public static void Seed(AppDbContext context)
        {
            //Products
            if (!context.Products.Any())
            {
                context.Products.AddRange(new List<Product>()
                {
                    new Product()
                    {
                        Id = 1,
                        PictureURL = "https://s7d1.scene7.com/is/image/mcdonalds/mcdonalds-McChicken-Sandwich-2:1-3-product-tile-desktop?wid=829&hei=515&dpr=off",
                        Name = "McChicken",
                        Description = "MacChicken description",
                        Category = Enums.ProductCategories.Burger,
                        Price = 30,
                    },
                    new Product()
                    {
                        Id = 2,
                        PictureURL = "https://s7d1.scene7.com/is/image/mcdonalds/DC_202002_6053_LargeFries_832x472:1-3-product-tile-desktop?wid=765&hei=472&dpr=off",
                        Name = "MacFries",
                        Description = "Cartofi fri",
                        Category = Enums.ProductCategories.Fries,
                        Price = 20,
                    },
                    new Product()
                    {
                        Id = 3,
                        PictureURL = "https://s3-eu-west-1.amazonaws.com/straus/media/products2/caa207626d9e43d2a5c29714047d1989.png",
                        Name = "Coca-Cola",
                        Description = "Nice Cola",
                        Category = Enums.ProductCategories.Drinks,
                        Price = 15,
                    }

                });
            }
            //Menu
            if (!context.Menus.Any())
            {
                context.Menus.AddRange(new List<Menu>()
                {
                new Menu()
                {
                    Id= 1,
                    Name = "McChicken Menu",
                    PictureURL = "https://mcdonalds.md/image/catalog/products/meals/MeniuMcChickenThumb-21.png",
                    Price = 90,
                    RestaurantId = 1,
                    Category = Enums.BaseCategories.FastFood
                }
                });
            }
            //Products & Menues
            if (!context.Products_Menues.Any())
            {
                context.Products_Menues.AddRange(new List<Product_Menu>()
                {
                new Product_Menu()
                {
                    MenuId = 1,
                    ProductId= 1,
                },
                new Product_Menu()
                {
                    MenuId = 1,
                    ProductId= 2,
                },
                new Product_Menu()
                {
                    MenuId = 1,
                    ProductId= 3,
                }
                });
            }
            //Restaurant
            if (!context.Restaurants.Any())
            {
                context.Restaurants.AddRange(new List<Restaurant>()
                {
                new Restaurant()
                {
                    Id = 1,
                    Name = "McDonalds",
                    PictureURL = "https://gastronomiaycia.republica.com/wp-content/uploads/2015/05/aniversario_mcdonalds.jpg",
                    Category = "Burger",
                }
                });
            }

            if (!context.Users.Any())
            {
                context.Users.AddRange(new List<UserData>()
                {
                new UserData()
                {
                    Email = "twinsAdmin@mail.ru",
                    Password = "D975177EBD877C3CF278A7EF06672527B40F23D0151D95325092C2DEF1E3349C",
                    Username = "Admin",
                    Role = Enums.Roles.admin
                }
                });
            }
            context.SaveChanges();
        }
    }
}
