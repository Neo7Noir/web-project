﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Entityes
{
    public class Restaurant
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]

        [Display(Name = "Id")]
        [Required(ErrorMessage = "Id is required")]
        [Key] public int Id { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Display(Name = "Picture")]
        [Required(ErrorMessage = "Picture is required")]
        public string PictureURL { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Category is required")]
        public string Category { get; set; }

        //relation
        public List<Menu> Menues { get; set; }
    }
}
