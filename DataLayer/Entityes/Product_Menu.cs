﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entityes
{
    public class Product_Menu
    {
        public int MenuId { get; set; }
        public Menu Menu { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
