﻿using DataLayer.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entityes
{
    public class CartItem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]

        public int CartId { get; set; }
        public Cart Cart { get; set; }

        public int MenuId { get; set; }
        public Menu Menu { get; set; }
        public int Amount { get; set; }

    }
}
