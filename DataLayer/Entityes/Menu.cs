﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using DataLayer.Enums;

namespace DataLayer.Entityes
{
    public class Menu
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]

        [Display(Name = "Id")]
        [Required(ErrorMessage = "Id is required")]
        [Key] public int Id { get; set; }

        [Display(Name = "Picture")]
        [Required(ErrorMessage = "Picture is required")]
        public string PictureURL { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Display(Name = "Price")]
        [Required(ErrorMessage = "Price is required")]
        public double Price { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Category is required")]
        public BaseCategories Category { get; set; }

        //relation
        public List<Product_Menu> Products_Menues { get; set; }

        public List<CartItem> CartItems { get; set; }

        //Restaurant
        public int RestaurantId { get; set; }
        [ForeignKey("RestaurantId")]
        public Restaurant Restaurant { get; set; }
    }
}
