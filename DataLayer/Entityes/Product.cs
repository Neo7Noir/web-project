﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using DataLayer.Enums;

namespace DataLayer.Entityes
{
    public class Product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]

        [Display(Name = "Id")]
        [Required(ErrorMessage = "Id is required")]
        [Key] public int Id { get; set; }

        [Display(Name = "Picture")]
        [Required(ErrorMessage = "Picture is required")]
        public string PictureURL { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is required")]
        [StringLength(50, MinimumLength = 4, ErrorMessage = "Name must be between 3 and 50 characters")]
        public string Name { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Category is required")]
        public ProductCategories Category { get; set; }

        [Display(Name = "Price")]
        [Required(ErrorMessage = "Price is required")]
        public double Price { get; set; }

        //relations
        public List<Product_Menu>? Products_Menues { get; set; }
    }
}
